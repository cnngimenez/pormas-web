<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 */
class TarjetaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
	'css/tarjeta.css',
        'css/elementor.css',
	'css/elementor2.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
