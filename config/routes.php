<?php

return [
    'voucher' => 'voucher/index',
    'colaborador' => 'colaborador/index',
    'ciudad' => 'ciudad/index',
    'rubro' => 'rubro/index',
    '<razon_social>' => 'colaborador/tarjeta',
    'tarjeta/<id:\d+>' => 'colaborador/tarjeta',
];

?>
