<?php

namespace app\controllers;

use Yii;
use app\models\Voucher;
use app\models\VoucherSearch;
use app\models\Colaborador;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * VoucherController implements the CRUD actions for Voucher model.
 */
class VoucherController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['update', 'create', 'delete',
				     'index', 'view'],
			'roles' => ['voucher-management'],
		    ],

		],
	    ],
	];
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex()
    {
	$searchModel = new VoucherSearch();
	if (!Yii::$app->user->can('user-management')){
	    $colab = Colaborador::find_by_user(Yii::$app->user);
	    $searchModel->colaborador_id = $colab->id;
	}
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
 	$colab = Colaborador::find_by_user(Yii::$app->user);

	return $this->render('index', [
	    'colab' => $colab,
	    'searchModel' => $searchModel,
	    'dataProvider' => $dataProvider,
	]);
    }

    /**
     * Displays a single Voucher model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	return $this->render('view', [
	    'model' => $this->findModel($id),
	]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	$current_colab = Colaborador::find_by_user(Yii::$app->user);
	if (!$current_colab->puede_crear_voucher()){
	    return $this->redirect(['index']);
	}
	$model = new Voucher();
	$model->colaborador_id = $current_colab->id;

	if ($model->load(Yii::$app->request->post())){

	    // Subir la imagen del voucher
	    $imagen = UploadedFile::getInstance($model, 'foto_image');
	    if ($imagen != null){
		$model->foto_image = $imagen;
		$model->upload_foto();
	    }
	    
	    if ($model->save()) {
		return $this->redirect(['view', 'id' => $model->id]);
	    }
	}

	return $this->render('create', [
	    'model' => $model,
	]);
    }

    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
	$model = $this->findModel($id);
	$current_colab = Colaborador::find_by_user(Yii::$app->user);
	if ($model->colaborador_id != $current_colab->id &&
	    ! Yii::$app->user->can('user-management')){
	    return $this->redirect(['index']);
	}
	
	if ($model->load(Yii::$app->request->post())){

	    // Subir la imagen del voucher
	    $imagen = UploadedFile::getInstance($model, 'foto_image');
	    if ($imagen != null){
		$model->foto_image = $imagen;
		$model->upload_foto();
	    }

	    if ($model->save()) {
		return $this->redirect(['view', 'id' => $model->id]);
	    }
	}

	return $this->render('update', [
	    'model' => $model,
	]);
    }

    /**
     * Deletes an existing Voucher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
	$model = $this->findModel($id);
	$current_colab = Colaborador::find_by_user(Yii::$app->user);
	if ($model->colaborador_id != $current_colab->id &&
	    ! Yii::$app->user->can('user-management')){
	    return $this->redirect(['index']);
	}

	$model->delete();

	return $this->redirect(['index']);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
	if (($model = Voucher::findOne($id)) !== null) {
	    return $model;
	}

	throw new NotFoundHttpException('The requested page does not exist.');
    }
}
