<?php

namespace app\controllers;

use Yii;
use app\models\Colaborador;
use app\models\ColaboradorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * ColaboradorController implements the CRUD actions for Colaborador model.
 */
class ColaboradorController extends Controller
{

    /**
     */
    public function actions(){
	return [
	    'captcha' => [
		'class' => \yii\captcha\CaptchaAction::className(),
	    ]
	];
    } // actions
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
	    ],

	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['tarjeta', 'create'],
			'roles' => [],
		    ],
		    [
			'allow' => true,
			'actions' => ['tarjeta', 'update', 'create'],
			'roles' => ['colaborador-update', 'colaborador-create'],
		    ],
		    [
			'allow' => true,
			'actions' => ['index', 'view', 
				     'update', 'create', 'delete'],
			'roles' => ['colaborador-management'],
		    ],
		    [
			'allow' => true,
			'actions' => ['publicar'],
			'roles' => ['colaborador-publicar'],
		    ],
	    ]],
        ];
    }

    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ColaboradorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Colaborador model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	$model = $this->findModel($id);
	
        return $this->render('view', [
	    'model' => $model
        ]);	
    }

    public function actionTarjeta($id=null, $razon_social=null)
    {
	$razon_social = str_replace('_', ' ', $razon_social);
	$razon_social = str_replace('-', ' ', $razon_social);
	
	if ($razon_social != null) {
	    $model = Colaborador::find()
				->where(['razon_social' => $razon_social])
				->one();
	    if ($model == null){
		throw new NotFoundHttpException('The requested page does not exist.');
	    }
	}
	
	if ($id != null){
	    $model = $this->findModel($id);
	}
	
	if ($model->publico || Yii::$app->user->can('colaborador-publicar')){
	    $this->layout = 'tarjeta';
            return $this->render('tarjeta', [
		'model' => $model,
            ]);
	}else{
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

    /**
     * Creates a new Colaborador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Colaborador(['scenario' => Colaborador::CREATING_SCENARIO]);
	$model->publico = false;

        if ($model->load(Yii::$app->request->post())){

	    // Subir la imagen de la marca
	    $imagen = UploadedFile::getInstance($model, 'marca_image');
	    if ($imagen != null){
		$model->marca_image = $imagen; 
		$model->upload_marca();
	    }

	    // Asignar las ciudades seleccionadas
	    $model->asignar_ciudades(Yii::$app->request->post('ciudades'));
	    
 	    if ($model->save()) {
		// return $this->redirect(['view', 'id' => $model->id]);
		return $this->goHome();
	    }
	}

        return $this->render('create', [
	    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Colaborador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){

	    $imagen = UploadedFile::getInstance($model, 'marca_image');
	    if ($imagen != null){
		$model->marca_image = $imagen; 
		$model->upload_marca();
	    }

	    /*
	       if (! Yii::$app->user->can('colaborador_change_user_id')){
	       // Asignarle el usuario_id si no puede cambiarlo (no es Admin)
	       $model->usuario_id = Yii::$app->user->id;
	       }
	     */

	    $model->asignar_ciudades(Yii::$app->request->post('ciudades'));
	    
	    if ($model->save()) {
		return $this->redirect(['tarjeta', 'id' => $model->id]);
	    }

        }

        return $this->render('update', [
	    'model' => $model,
        ]);
    }

    public function actionPublicar($id, $value)
    {
        $model = $this->findModel($id);
	
	// No hace falta chequear permisos porque se hace en behaviours()
	$model->publico = $value;

	if ($model->publico && ($model->usuario_id == null)){
	    // Asignarle el usuario_id si no lo tiene
	    if (!$model->crear_usuario()){
		throw new \Exception("There's a problem creating the card");
	    }
	}
	
	if (!$model->save()){
	    throw new \Exception("There's a problem seting the card public");
	}
	
        return $this->redirect(['view', 'id' => $model->id]);
    }


    /**
     * Deletes an existing Colaborador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Colaborador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Colaborador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Colaborador::findOne($id)) !== null) {
	    return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
