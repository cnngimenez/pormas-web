<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubro}}`.
 */
class m200422_215507_create_rubro_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubro}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubro}}');
    }
}
