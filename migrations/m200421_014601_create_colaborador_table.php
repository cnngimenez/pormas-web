<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colaborador}}`.
 */
class m200421_014601_create_colaborador_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%colaborador}}', [
            'id' => $this->primaryKey(),
            'razon_social' => $this->string(),
            'ciudad' => $this->string(),
            'dni' => $this->string(),
            'marca' => $this->string(),
            'actividad' => $this->string(),
            'descripcion' => $this->text(),
            'introduccion' => $this->text(),
            'correo' => $this->string(),
            'tel' => $this->string(),
            'mercado_pago' => $this->boolean(),
            'whatsapp' => $this->string(),
            'facebook' => $this->string(),
            'instagram' => $this->string(),
	    'usuario_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%colaborador}}');
    }
}
