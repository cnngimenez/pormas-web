<?php

use yii\db\Migration;

/**
 * Class m200422_222628_agregar_rbac_rubro_y_ciudad
 */
class m200422_222628_agregar_rbac_rubro_y_ciudad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;

	$admin = $auth->getRole('Admin');

	$perm = $auth->createPermission('rubro-management');
	$perm->description = 'Gestiona completamente los rubros';
	$auth->add($perm);
	$auth->addChild($admin, $perm);
	
	$perm = $auth->createPermission('ciudad-management');
	$perm->description = 'Gestiona completamente las ciudades';
	$auth->add($perm);
	$auth->addChild($admin, $perm);
	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200422_222628_agregar_rbac_rubro_y_ciudad cannot be reverted.\n";

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_222628_agregar_rbac_rubro_y_ciudad cannot be reverted.\n";

       return false;
       }
     */
}
