<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%colaborador}}`.
 */
class m200509_190801_add_publico_column_to_colaborador_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%colaborador}}', 'publico', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%colaborador}}', 'publico');
    }
}
