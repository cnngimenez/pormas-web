<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%colaborador}}`.
 */
class m200426_012601_add_rubro_id_column_to_colaborador_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%colaborador}}', 'rubro_id', $this->integer());
	$this->addForeignKey(
	    'fk-colaborador-rubro',
	    'colaborador', 'rubro_id',
	    'rubro', 'id',
	    'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$this->dropForeignKey('fk-colaborador-rubro', 'colaborador');
        $this->dropColumn('{{%colaborador}}', 'rubro_id');
    }
}
