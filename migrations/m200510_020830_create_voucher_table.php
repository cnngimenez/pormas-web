<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%voucher}}`.
 */
class m200510_020830_create_voucher_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%voucher}}', [
            'id' => $this->primaryKey(),
            'servicio' => $this->string(),
            'precio' => $this->float(),
            'descripcion' => $this->string(),
            'mercado_pago' => $this->string(),
            'con_promo' => $this->boolean(),
            'promo_descuento' => $this->float(),
            'foto' => $this->string(),
            'colaborador_id' => $this->integer(),
        ]);

	$this->addForeignKey(
	    'fk-voucher-colaborador',
	    'voucher', 'colaborador_id',
	    'colaborador', 'id',
	    'RESTRICT', 'CASCADE');
	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%voucher}}');
    }
}
