<?php

use yii\db\Migration;
use Da\User\Model\User;

/**
 * Class m200422_015229_default_users
 */
class m200422_015229_default_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;
	
	$admin = new User();
	$admin->id = 1;
	$admin->username = 'admin';
	$admin->password = 'admin1234';
	$admin->email = 'admin@localhost.localhost';
	$admin->save();

	$colab = new User();
	$colab->id = 2;
	$colab->username = 'colab';
	$colab->password = 'colab1234';
	$colab->email = 'colaborador@localhost.localhost';
	$colab->save();

	$adminrole = $auth->getRole('Admin');
	$colabrole = $auth->getRole('Colaborador');	
	$auth->assign($adminrole, $admin->id);
	$auth->assign($colabrole, $colab->id);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$users = User::find()->all();

	foreach ($users as $user){
	    echo $user->id;
	    $user->delete();
	}
	
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_015229_default_users cannot be reverted.\n";

       return false;
       }
     */
}
