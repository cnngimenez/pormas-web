<?php

use yii\db\Migration;

/**
 * Class m200510_025035_add_voucher_perms
 */
class m200510_025035_add_voucher_perms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;

	$admin = $auth->getRole('Admin');
	$colab = $auth->getRole('Colaborador');

	$manag = $auth->createPermission('voucher-management');
        $manag->description = 'Gestiona completamente sus vouchers.';
        $auth->add($manag);
	$auth->addChild($admin, $manag);
	$auth->addChild($colab, $manag);

	// Los siguientes no importan mucho, pero los agrego por las dudas.
	
	$perm = $auth->createPermission('voucher-create');
        $perm->description = 'Crear un voucher.';
        $auth->add($perm);
	$auth->addchild($manag, $perm);

	$perm = $auth->createPermission('voucher-view');
        $perm->description = 'Ver o listar vouchers propios.';
        $auth->add($perm);
	$auth->addchild($manag, $perm);

	$perm = $auth->createPermission('voucher-update');
        $perm->description = 'Actualizar un voucher propio.';
        $auth->add($perm);
	$auth->addchild($manag, $perm);

	$perm = $auth->createPermission('voucher-delete');
        $perm->description = 'Borrar un voucher propio.';
        $auth->add($perm);
	$auth->addchild($manag, $perm);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$auth = Yii::$app->authManager;

	$auth->remove('voucher-management');
	$auth->remove('voucher-create');
	$auth->remove('voucher-view');
	$auth->remove('voucher-update');
	$auth->remove('voucher-delete');

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200510_025035_add_voucher_perms cannot be reverted.\n";

       return false;
       }
     */
}
