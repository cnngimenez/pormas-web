<?php

use yii\db\Migration;
use app\models\Rubro;

/**
 * Class m200422_215553_agregar_rubros_por_defecto
 */
class m200422_215553_agregar_rubros_por_defecto extends Migration
{

    public $lst_rubros = [
	'Alimentos',
	'Artistas',
	'Belleza y Cuidado Personal',
	'Creaciones de Autor',
	'Deporte',
	'Eventos',
	'Fotografía',
	'Indumentaria',
	'Logística y Envíos',
	'Mantenimiento del Hogar',
	'Mascotas',
	'Productos Sustentables',
	'Profesores',
	'Regalos',
	'Salud',
	'Seguros',
	'Servicio Técnico y Reparaciones',
	'Servicios de Gráfica e Impresiones',
	'Servicios Profesionales',
	'Trámites',
	'Ventas',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	foreach ($this->lst_rubros as $id => $nombre){
	    $rubro = new Rubro();
	    $rubro->nombre = $nombre;
	    $rubro->save();
	}
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	foreach ($this->lst_rubros as $nombre){
	    $rubro = Rubro::find()->where(['nombre' => $nombre])->one();
	    $rubro->delete();	    
	}

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_215553_agregar_rubros_por_defecto cannot be reverted.\n";

       return false;
       }
     */
}
