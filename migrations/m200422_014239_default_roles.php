<?php

use yii\db\Migration;

/**
 * Class m200422_014239_default_roles
 */
class m200422_014239_default_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;

	// Create roles
	$admin = $auth->getRole('Admin');
	if ($admin == null){
	    $admin = $auth->createRole('Admin');
            $auth->add($admin);
	}
	$guest = $auth->getRole('Guest');
	if ($guest == null){
	    $guest = $auth->createRole('Guest');
            $auth->add($guest);
	}
	$colab = $auth->getRole('Colaborador');
	if ($colab == null){
	    $colab = $auth->createRole('Colaborador');
            $auth->add($colab);
	}
	
	// Un colaborador puede hacer lo mismo que un invitado y más.
	$auth->addChild($colab, $guest);
	// Un admin puede hacer lo mismo que un colaborador y más.
	$auth->addChild($admin, $colab);

	
	// Permisos para Colaboradores (Tarjetas)
	$colab_mang = $auth->createPermission('colaborador-management');
        $colab_mang->description = 'Gestiona completamente los Colaboradores (Tarjetas).';
        $auth->add($colab_mang);
	$auth->addChild($admin, $colab_mang);

	$perm = $auth->createPermission('colaborador-create');
        $perm->description = 'Crear un Colaborador (Tarjeta).';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);
	$auth->addChild($colab, $perm);
	
	$perm = $auth->createPermission('colaborador-view');
        $perm->description = 'Ver un Colaborador (Tarjeta).';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);
	$auth->addChild($guest, $perm);

	$perm = $auth->createPermission('colaborador-update');
        $perm->description = 'Editar un Colaborador (Tarjeta).';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);
	$auth->addChild($colab, $perm);

	$perm = $auth->createPermission('colaborador-delete');
        $perm->description = 'Borrar un Colaborador (Tarjeta).';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);

	$perm = $auth->createPermission('colaborador-change_user_id');
        $perm->description = 'Cambiar el usuario asignado a un Colaborador (Tarjeta).';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$auth = Yii::$app->authManager;

        $auth->removeAll();
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_014239_default_roles cannot be reverted.\n";

       return false;
       }
     */
}
