<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ciudad}}`.
 */
class m200422_215449_create_ciudad_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ciudad}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ciudad}}');
    }
}
