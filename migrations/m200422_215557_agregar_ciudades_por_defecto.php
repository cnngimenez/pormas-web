<?php

use yii\db\Migration;
use app\models\Ciudad;

/**
 * Class m200422_215557_agregar_ciudades_por_defecto
 */
class m200422_215557_agregar_ciudades_por_defecto extends Migration
{

    public $lst_ciudades = [
	'Servicios Online',
	'Ciudad',
	'Allen',
	'Bariloche',
	'Catriel',
	'Centenario',
	'Cinco Saltos',
	'Cipolletti',
	'Fernández Oro',
	'General Conesa',
	'General Roca',
	'Junin de los Andes',
	'Lamarque',
	'Neuquén Capital',
	'Plottier',
	'Rincón de los sauces',
	'Villa Regina',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	
	foreach ($this->lst_ciudades as $id => $nombre){
	    $ciudad = new Ciudad();
	    $ciudad->nombre = $nombre;
	    $ciudad->save();
	}
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	foreach ($this->lst_ciudades as $nombre){
	    $ciudad = Ciudad::find()->where(['nombre' => $nombre])->one();
	    $ciudad->delete();	    
	}

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_215557_agregar_ciudades_por_defecto cannot be reverted.\n";

       return false;
       }
     */
}
