<?php

use yii\db\Migration;

/**
 * Class m200509_191538_add_publicar_perms
 */
class m200509_191538_add_publicar_perms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;

	// $admin = $auth->getRole('Admin');
	$colab_mang = $auth->getPermission('colaborador-management');
	

	$perm = $auth->createPermission('colaborador-publicar');
        $perm->description = 'Poder mostrar u ocultar tarjetas del público. ' .
			    'En otras palabral, probar tarjetas para ser publicadas.';
        $auth->add($perm);
	$auth->addChild($colab_mang, $perm);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

	$auth->remove('colaborador-publicar');
	
        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200509_191538_add_publicar_perms cannot be reverted.\n";

       return false;
       }
     */
}
