<?php

use yii\db\Migration;

/**
 * Class m200422_223019_agregar_user_rbac
 */
class m200422_223019_agregar_user_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$auth = Yii::$app->authManager;

	$admin = $auth->getRole('Admin');

	$perm = $auth->createPermission('user-management');
	$perm->description = 'Gestiona completamente los usuarios';
	$auth->add($perm);
	$auth->addChild($admin, $perm);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200422_223019_agregar_user_rbac cannot be reverted.\n";

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200422_223019_agregar_user_rbac cannot be reverted.\n";

       return false;
       }
     */
}
