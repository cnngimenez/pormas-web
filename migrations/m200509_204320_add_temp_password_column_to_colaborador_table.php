<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%colaborador}}`.
 */
class m200509_204320_add_temp_password_column_to_colaborador_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%colaborador}}', 'temp_password', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%colaborador}}', 'temp_password');
    }
}
