<?php

use yii\db\Migration;

/**
 * Class m200421_021020_add_foreign_key_to_colaborador
 */
class m200421_021020_add_foreign_key_to_colaborador extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$this->addForeignKey(
	    'fk-colaborador-user',
	    'colaborador', 'usuario_id',
	    'user', 'id',
	    'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$this->dropForeignKey('fk-colaborador-user', 'colaborador');

        return false;
    }

    /*
       // Use up()/down() to run migration code without a transaction.
       public function up()
       {

       }

       public function down()
       {
       echo "m200421_021020_add_foreign_key_to_colaborador cannot be reverted.\n";

       return false;
       }
     */
}
