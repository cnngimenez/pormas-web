<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colaborador_ciudad}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%colaborador}}`
 * - `{{%ciudad}}`
 */
class m200426_014223_create_junction_table_for_colaborador_and_ciudad_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%colaborador_ciudad}}', [
            'colaborador_id' => $this->integer(),
            'ciudad_id' => $this->integer(),
            'PRIMARY KEY(colaborador_id, ciudad_id)',
        ]);

        // creates index for column `colaborador_id`
        $this->createIndex(
            '{{%idx-colaborador_ciudad-colaborador_id}}',
            '{{%colaborador_ciudad}}',
            'colaborador_id'
        );

        // add foreign key for table `{{%colaborador}}`
        $this->addForeignKey(
            '{{%fk-colaborador_ciudad-colaborador_id}}',
            '{{%colaborador_ciudad}}',
            'colaborador_id',
            '{{%colaborador}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ciudad_id`
        $this->createIndex(
            '{{%idx-colaborador_ciudad-ciudad_id}}',
            '{{%colaborador_ciudad}}',
            'ciudad_id'
        );

        // add foreign key for table `{{%ciudad}}`
        $this->addForeignKey(
            '{{%fk-colaborador_ciudad-ciudad_id}}',
            '{{%colaborador_ciudad}}',
            'ciudad_id',
            '{{%ciudad}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%colaborador}}`
        $this->dropForeignKey(
            '{{%fk-colaborador_ciudad-colaborador_id}}',
            '{{%colaborador_ciudad}}'
        );

        // drops index for column `colaborador_id`
        $this->dropIndex(
            '{{%idx-colaborador_ciudad-colaborador_id}}',
            '{{%colaborador_ciudad}}'
        );

        // drops foreign key for table `{{%ciudad}}`
        $this->dropForeignKey(
            '{{%fk-colaborador_ciudad-ciudad_id}}',
            '{{%colaborador_ciudad}}'
        );

        // drops index for column `ciudad_id`
        $this->dropIndex(
            '{{%idx-colaborador_ciudad-ciudad_id}}',
            '{{%colaborador_ciudad}}'
        );

        $this->dropTable('{{%colaborador_ciudad}}');
    }
}
