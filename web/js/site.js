function actualizar_masonry () {
    $("#w1 .grid").masonry();
}

$(function () {
    $(document).on('ready pjax:success',
		   function (event) {
		       window.setTimeout(actualizar_masonry, 1000);
		   });
    window.setTimeout(actualizar_masonry, 1000);
});
