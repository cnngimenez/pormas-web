<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Colaborador;

/**
 * ColaboradorSearch represents the model behind the search form of `app\models\Colaborador`.
 */
class ColaboradorSearch extends Colaborador
{
    public $ciudad_servicio_id = null;
    public $texto = null;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rubro_id', 'mercado_pago', 'usuario_id',
	      'ciudad_servicio_id',
	    ], 'integer'],
	    [['publico'], 'boolean'],
            [['razon_social', 'ciudad', 'dni', 'marca', 'actividad',
	      'descripcion', 'introduccion', 'correo', 'tel', 'whatsapp', 'facebook',
	      'instagram', 'texto',
	      'rubro.nombre'], 'safe'],
        ];
    }
    /**
     */
    public function attributes(){
	return array_merge(parent::attributes(), [
	    'rubro.nombre',
	]);
    } // attributes

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Colaborador::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any
	    // records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mercado_pago' => $this->mercado_pago,
            'usuario_id' => $this->usuario_id,
        ]);

	if ($this->rubro_id != "-1") {
            $query->andFilterWhere(['rubro_id' => $this->rubro_id]);
	}

	if ($this->ciudad_servicio_id != -1){
	    $query->join('LEFT JOIN', 'colaborador_ciudad',
			'colaborador.id = colaborador_ciudad.colaborador_id');
	    $query->andFilterWhere([
		'colaborador_ciudad.ciudad_id' => $this->ciudad_servicio_id]);
	}

	$query->joinWith('rubro');

        $query->andFilterWhere(['like', 'razon_social', $this->razon_social])
             ->andFilterWhere(['like', 'ciudad', $this->ciudad])
	     ->andFilterWhere(['like', 'publico', $this->publico])
             ->andFilterWhere(['like', 'dni', $this->dni])
             ->andFilterWhere(['like', 'marca', $this->marca])
             ->andFilterWhere(['like', 'actividad', $this->actividad])
             ->andFilterWhere(['like', 'descripcion', $this->descripcion])
             ->andFilterWhere(['like', 'introduccion', $this->introduccion])
             ->andFilterWhere(['like', 'correo', $this->correo])
             ->andFilterWhere(['like', 'tel', $this->tel])
             ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
             ->andFilterWhere(['like', 'facebook', $this->facebook])
             ->andFilterWhere(['like', 'instagram', $this->instagram]);

	if ($this->texto != '' and $this->texto != null){
	    $sql = "(actividad LIKE :texto OR " .
		   "razon_social LIKE :texto OR " .
		   "descripcion LIKE :texto OR " .
		   "ciudad LIKE :texto)";
	    $query->andWhere($sql,
			    [":texto" => '%' . $this->texto . '%']);
	}

	$query->andFilterWhere(['like', 'rubro.nombre',
			       $this->getAttribute('rubro.nombre')]);
	
	$dataProvider->sort->attributes['rubro.nombre'] = [
	    'asc' => ['rubro.nombre' => SORT_ASC],
	    'desc' => ['rubro.nombre' => SORT_DESC],
	];

	return $dataProvider;
    }
}
