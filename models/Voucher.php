<?php

namespace app\models;
use yii\helpers\Url;


use Yii;

/**
 * This is the model class for table "voucher".
 *
 * @property int $id
 * @property string|null $servicio
 * @property float|null $precio
 * @property string|null $descripcion
 * @property string|null $mercado_pago
 * @property int|null $con_promo
 * @property float|null $promo_descuento
 * @property string|null $foto
 * @property int|null $colaborador_id
 *
 * @property Colaborador $colaborador
 */
class Voucher extends \yii\db\ActiveRecord
{

    /**
       El objeto imagen de la foto.
     */
    public $foto_image;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'voucher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'promo_descuento'], 'number'],
            [['con_promo', 'colaborador_id'], 'integer'],
            [['servicio', 'descripcion', 'mercado_pago', 'foto'],
	     'string', 'max' => 255],
	    [['foto_image'], 'image',
	     'skipOnEmpty' => true,
	     'extensions' => 'png, jpg, jpeg, svg',
	     'message' => 'Sólo se aceptan los formatos PNG, JPG y SVG'],
            [['colaborador_id'], 'exist',
	     'skipOnError' => true,
	     'targetClass' => Colaborador::className(),
	     'targetAttribute' => ['colaborador_id' => 'id']],

	    [['precio', 'servicio', 'descripcion'], 'required',
	     'message' => 'Este campo no puede estar en blanco.'],
	    [['promo_descuento'], 'required', 'when' =>
		function ($model) {
		    return $model->con_promo;
		},
	     'whenClient' =>
		 'function (attribute, value){
                      return $("#voucher-con_promo")[0].checked;
                  }',
	     'message' => 'Si es con promo, debe indicar un precio de promoción.']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio' => 'Nombre del servicio o producto',
            'precio' => 'Precio',
            'descripcion' => 'Descripción',
            'mercado_pago' => 'Link de Mercado Pago',
            'con_promo' => '¿Con Promo?',
            'promo_descuento' => 'Indique el precio de promoción',
            'foto' => 'Foto',
	    'foto_image' => 'Foto',
            'colaborador_id' => 'Colaborador ID',
        ];
    }

    /**
     */
    public function upload_foto(){
	if ($this->foto_image == null){
	    $this->foto = null;
	    return true;
	}
	if ($this->validate()){

	    $this->foto = $this->foto_image->baseName . '-' .
			  uniqid() . '.' .
			  $this->foto_image->extension;
	    $filename = '../web/upload_vouchers/' . $this->foto;

	    $this->foto_image->saveAs($filename);
	    
	    $this->foto_image = null;
	    return true;
	}else{
	    return false;
	}
    } // upload_foto

    
    /**
     * Gets query for [[Colaborador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColaborador()
    {
        return $this->hasOne(Colaborador::className(), ['id' => 'colaborador_id']);
    }

    /**
     */
    public function getFoto_URL(){
	if ($this->foto != null){
	    return Url::to('@web/upload_vouchers/' . $this->foto);
	}
	return null;
    } // getFoto_URL

}
