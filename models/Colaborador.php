<?php

namespace app\models;

use Yii;
use Da\User\Model\User;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * This is the model class for table "colaborador".
 *
 * @property int $id
 * @property boolean|null $publico Si el perfil fue aprobado para ser publicado
 * @property string|null $temp_password Contraseña que el usuario brinda para
 *   hacer su registro. Luego será guardada en User cuando éste se inicialice.
 * @property string|null $razon_social
 * @property string|null $ciudad
 * @property string|null $dni
 * @property string|null $marca
 * @property string|null $actividad
 * @property string|null $descripcion
 * @property string|null $introduccion
 * @property string|null $correo
 * @property string|null $tel
 * @property int|null $mercado_pago
 * @property string|null $whatsapp
 * @property string|null $facebook
 * @property string|null $instagram
 * @property int|null $usuario_id
 *
 * @property User $usuario
 */
class Colaborador extends \yii\db\ActiveRecord
{

    public $marca_image;
    public $verifyCode;

    const CREATING_SCENARIO = 'creating';
    const DEFAULT_SCENARIO = 'default';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'colaborador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
	    [['temp_password'], 'string', 'min' => 5, 'max' => 20,
	     'tooShort' => 'La clave debe tener al menos 5 caracteres.',
	     'tooLong' => 'La clave no debe superar los 20 caracteres.'],
	    ['verifyCode', 'captcha', 'on' => Colaborador::CREATING_SCENARIO],
	    [['temp_password'], 'required', 'on' => colaborador::CREATING_SCENARIO],
	    
	    [['descripcion', 'introduccion'], 'string'],
            [['usuario_id', 'rubro_id'], 'integer'],
	    [['publico', 'mercado_pago'], 'boolean'],
	    [['marca_image'], 'image',
	     'skipOnEmpty' => true,
	     'extensions' => 'png, jpg, jpeg, svg',
	     'message' => 'Sólo se aceptan el formato PNG, JPG, SVG'],
            [['razon_social', 'ciudad', 'dni', 'marca',
	      'actividad', 'tel', 'whatsapp',
	      'facebook', 'instagram'], 'string', 'max' => 255],
	    [['correo'], 'email',
	     'message' => 'El correo tiene un formato inválido'],
	    [['razon_social', 'ciudad', 'dni',
	      'actividad', 'descripcion', 'correo', 'tel',
	      'mercado_pago', 'whatsapp', 'rubro_id',
	    ], 'required',
	     'message' => 'Este campo es obligatorio'],
            [['usuario_id'], 'exist',
	     'skipOnError' => true,
	     'targetClass' => User::className(),
	     'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	    'verifyCode' => 'CAPTCHA',
	    'temp_password' => 'Contraseña de sesión',
	    'publico' => 'Público (aprobado)',
            'id' => 'ID',
            'razon_social' => 'Razón Social',
            'ciudad' => 'Ciudad',
            'dni' => 'DNI',
            'marca' => 'Marca',
            'actividad' => 'Actividad',
            'descripcion' => 'Descripción',
            'introduccion' => 'Introducción',
            'correo' => 'Correo',
            'tel' => 'Tel.',
            'mercado_pago' => 'Mercado Pago',
            'whatsapp' => 'Whatsapp',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'usuario_id' => 'Usuario ID',
        ];
    }

    /**
     */
    public function getMarca_URL(){
	if ($this->marca != null){
	    return Url::to('@web/upload_marcas/' . $this->marca);
	}else{
	    return Url::to('@web/imgs/cropped-favicon-180x180.png');
	}
    } // getMarcaUrl
    
    /**
     */
    public function upload_marca(){
	if ($this->marca_image == null){
	    $this->marca = null;
	    return true;
	}
	if ($this->validate()){

	    $this->marca = $this->marca_image->baseName . '-' .
			  uniqid() . '.' .
			  $this->marca_image->extension;
	    $filename = '../web/upload_marcas/' . $this->marca;

	    $this->marca_image->saveAs($filename);
	    
	    $this->marca_image = null;
	    return true;
	}else{
	    return false;
	}
    } // upload_marca
    
    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     */
    public function getRubro(){
	return $this->hasOne(Rubro::className(), ['id' => 'rubro_id']);
    } // getRubro

    /**
       Asignar las ciudades como aquellas que el colaborador presta servicios.

       @param $lst_ciudades array[string] Un listado de nombres de ciudades.
     */
    public function asignar_ciudades($lst_ciudades){
	if ($lst_ciudades == null){
	    return true;
	}
	$this->unlinkAll('ciudades', true);
	
	foreach ($lst_ciudades as $nombre){
	    $ciudad = Ciudad::find()->where(['id' => $nombre])->one();
	    if ($ciudad != null){
		$this->link('ciudades', $ciudad);
	    }
	}
    } // asignar_ciudades
    
    /**
     */
    public function getCiudades(){
	return $this->hasMany(Ciudad::className(), ['id' => 'ciudad_id'])
	      ->viaTable('colaborador_ciudad', ['colaborador_id' => 'id']);
    } // getCiudades

    /**
     */
    public function getVouchers(){
	return $this->hasMany(Voucher::className(), ['colaborador_id' => 'id']);
    } // getVouchers
    
    /**
       Crear el usuario que corresponde con este colaborador.

       Usar el DNI como su nombre de usuario.
       
       @return False si el usuario ya existe. True si se creó exitósamente.
     */
    public function crear_usuario(){
	$auth =  Yii::$app->authManager;
	
	if ($this->usuario_id != null){
	    return false;
	}

	$usuario = new User();
	$usuario->username = $this->dni;
	$usuario->password = $this->temp_password;
	$usuario->email = $this->correo;
	
	if (!$usuario->save()) {
	    throw new \Exception("Problems creating user for Colaborator");
	}

	// Asignar rol colaborador.
	$colabrole = $auth->getRole('Colaborador');	
	$auth->assign($colabrole, $usuario->id);

	// Usuario creado, asignarlo al colaborador
	$prevScen = $this->getScenario();
	$this->setScenario(Colaborador::DEFAULT_SCENARIO);
	
	$this->usuario_id = $usuario->id;
	if (!$this->save()){
	    throw new \Exception("Problems assigning user to colaborator:" .
				 var_export($this->getErrors(), true));	    
	}

	$this->setScenario($prevScen);
	
	return true;	
    } // crear_usuario

    /**
     */
    public function getFriendly_url(){
	return Url::to(['colaborador/tarjeta',
		  'razon_social' => str_replace(' ', '-', $this->razon_social)], true);
    } // get_friendly_url
    
    /**
     */
    public static function find_by_user($user){
	return Colaborador::find()->where(['usuario_id' => $user->id])->one();
    } // find_by_user

    /**
     */
    public function puede_crear_voucher(){
	return sizeof($this->vouchers) < 5;
    } // puede_crear_voucher


}
