<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Voucher;

/**
 * VoucherSearch represents the model behind the search form of `app\models\Voucher`.
 */
class VoucherSearch extends Voucher
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'con_promo', 'colaborador_id'], 'integer'],
            [['servicio', 'descripcion', 'mercado_pago', 'foto'], 'safe'],
            [['precio', 'promo_descuento'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Voucher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'precio' => $this->precio,
            'con_promo' => $this->con_promo,
            'promo_descuento' => $this->promo_descuento,
            'colaborador_id' => $this->colaborador_id,
        ]);

        $query->andFilterWhere(['like', 'servicio', $this->servicio])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'mercado_pago', $this->mercado_pago])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
