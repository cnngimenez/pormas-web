<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obtené tu tarjeta personal online totalmente gratis. Proyecto solidario que nació en Argentina para ayudar a emprendedores. ¡Entrá ahora!" />
    <meta name="keywords" content="Por Más,Proyecto solidario, ayudar emprendedores,Argentina" />
    <meta name="author"  content="Por Más pormas.org" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
    <?php
    NavBar::begin([
	'brandImage' => Url::to('@web/imgs/brand.png'),
	'brandOptions' => [ 'style' => 'padding-top: 5px' ],
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
	    'style' => 'background: radial-gradient(ellipse at bottom, #4bb3a3, #522e7e)',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
	    ['label' => 'Gestionar',
	     'options' => [
		 'style' => 'background-color: #400',
	     ],
	     'items' => [
		 ['label' => 'Vouchers', 'url' => ['/voucher'],
		  'visible' => Yii::$app->user->can('voucher-management'),
		 ],
		 ['label' => 'Tarjetas', 'url' => ['/colaborador'],
		  'visible' => Yii::$app->user->can('colaborador-management')],
		 ['label' => 'Ciudades', 'url' => ['/ciudad'],
		  'visible' => Yii::$app->user->can('ciudad-management')],
		 ['label' => 'Rubros', 'url' => ['/rubro'],
		  'visible' => Yii::$app->user->can('rubro-management')],
		 ['label' => 'Usuarios', 'url' => ['/user/admin'],
		  'visible' => Yii::$app->user->can('user-management')
		 ],
	     ],
	     'visible' => (Yii::$app->user->can('ciudad-management') ||
			  Yii::$app->user->can('rubro-management') ||
			  Yii::$app->user->can('user-management') ||
			  Yii::$app->user->can('colaborador-management') ||
			  Yii::$app->user->can('voucher-management'))
	    ],	     
            // ['label' => 'About', 'url' => ['/site/about']],
            // ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/user/security/login']]
            ) : (
                '<li>'
              . Html::beginForm(['/user/security/logout'], 'post')
              . Html::submitButton(
                  'Logout (' . Yii::$app->user->identity->username . ')',
                  ['class' => 'btn btn-link logout']
              )
              . Html::endForm()
              . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
      <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ]) ?>
      <?= Alert::widget() ?>
    </div>

    <?= $content ?>

    
    </div>

    <footer class="footer">
      <div class="container">
        <p class="pull-left">&copy; Por Más <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
      </div>
    </footer>

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>
