<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Da\User\Widget\ConnectWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View            $this
 * @var \Da\User\Form\LoginForm $model
 * @var \Da\User\Module         $module
 */

$this->context->layout = '/login';
$this->title = Yii::t('usuario', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;

use frontend\assets\AppAsset;
AppAsset::register($this);
?>

<?= $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="login-box login">
    <div class="login-logo" style="margin-top:-90px;margin-bottom: 0px">
        <a href="#"><img src="<?= Url::base() ?>/images/logo.png" alt="Fases" with="100%" /></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?= Html::encode($this->title) ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false]); ?>

        <?= $form
            ->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']])
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('login')]) ?>

        <?= $form
            ->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-7">
                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-5">
                <?= Html::submitButton(Yii::t('usuario', 'Sign in'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

        <?php if ($module->allowPasswordRecovery): ?>
             <?= Html::a(
                Yii::t('usuario', 'Forgot password?'),
                ['/user/recovery/request'],
                ['tabindex' => '5']
            ) ?><br>
        <?php endif; ?>

        <?php if ($module->enableEmailConfirmation): ?>
            <?= Html::a(
                Yii::t('usuario', 'Didn\'t receive confirmation message?'),
                ['/user/registration/resend']
            ) ?>
        <?php endif ?>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
