<?php

/* @var $model app\models\Voucher */

use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAR;

?>
<div class="container">
  

  <div class="voucher-outter">
    <div class="voucher-inner">

      <div class="row">
	

	<div class="col-md-6">
	  
	  <?php if  ($model->foto_URL != null){ ?>
	    <?= Html::img($model->foto_URL); ?>
	  <?php } ?>

	</div>

	<div class="col-md-6">
	  <div class="details">
	    <h1 class="subtitle"> <?= $model->servicio ?> </h1>

	    <?php if ($model->con_promo){ ?>
	      <p class="antes"> Antes: <?= $model->precio ?> </p>
	      <p class="precio"> Precio: <?= $model->promo_descuento ?> </p>
	    <?php }else{ ?>
	      <p class="precio">Precio: <?= $model->precio ?> </p>
	    <?php } ?>

	    <p class="desc"> <?= $model->descripcion ?></p>

	    <?= Html::a('COMPRAR ' . FAR::icon('arrow-alt-circle-right'),
		      $model->mercado_pago,
		      ['class' => 'comprar-link']) ?>

	  </div>
	</div>
      </div>
    </div>
  </div>
</div>
