<?php

/* @var $editing Hide captcha and field not usable for edition */
/* @var $this yii\web\View */
/* @var $model app\models\Colaborador */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use Da\User\Model\User;
use app\models\Rubro;
use app\models\Ciudad;
use dosamigos\selectize\SelectizeDropDownList;


if (!isset($editing)){
    $editing = false;
}

$ciudades_sel = [];
foreach ($model->ciudades as $ciudad){
    $ciudades_sel[] = $ciudad->id;
}
?>

<div class="colaborador-form">

  <?php $form = ActiveForm::begin(); ?>

  <?php
  if (!$editing){
      echo $form->field($model, 'temp_password')
	       ->passwordInput([
		   'maxlength' => true
	       ])
	       ->label('Contraseña de sesión ' .
		      '<span class="text-danger">*</span>');
  }
  ?>
  
  <?= $form->field($model, 'razon_social')
	 ->textInput([
	     'maxlength' => true,
	     'placeholder' => '"Juan Perez" o "Almacén de TITO"',
	 ])
	 ->label('Nombre y apellido (o nombre del negocio)
<span class="text-danger">*</span>')
  ?>

  <?= $form->field($model, 'ciudad')
	 ->textInput(['maxlength' => true])
	 ->label('¿En que ciudad vivís?
<span class="text-danger">*</span>')
  ?>

  <?= $form->field($model, 'dni')
	 ->textInput([
	     'maxlength' => true,
	     'placeholder' => 'ej. 12.345.678' 
	 ])
	 ->label('Número de DNI
<span class="text-danger">*</span>')
  ?>

  <?= Html::label('Foto de perfil o marca') ?>
  <?php
  if ($model->marca != null){
      echo Html::img($model->marca, [
	  'class' => 'img-circle img-responsive',
      ]);
      /*
      echo Html::checkbox('eliminar-img', false, [
	  'label' => 'Eliminar imagen', 
	 ]);
      */
  }else{
      echo "<p>Actualmente no tiene una imagen cargada.</p>";
      /*
	 echo Html::img('imgs/cropped-favicon-180x180.png', [
	 'class' => 'img-circle img-responsive']);
       */
  }
  ?>
  
  <?= $form->field($model, 'marca_image')->fileInput()->label(false); ?>

  <?= $form->field($model, 'actividad')
	 ->textInput([
	     'maxlength' => true,
	     'placeholder' => 'A qué te dedicás, tiene que ser breve'
	 ])
	 ->label('Actividad
<span class="text-danger">*</span>')
  ?>

  <?= $form->field($model, 'rubro_id')->dropdownList(
      Rubro::find()->select(['nombre'])
	   ->indexBy('id')
	   ->column()
  )->label('Rubro
<span class="text-danger">*</span>'); ?>

  <?= $form->field($model, 'descripcion')
	 ->textarea([
	     'rows' => 6,
	     'placeholder' => 'Informar bien de qué se trata el servicio/producto'
	 ])
	 ->label('Descripción detallada de servicios incluido horarios
<span class="text-danger">*</span>')
  ?>


  <?= Html::label('¿A que ciudades prestas tu servicio? Selección múltiple');?>
  <?= SelectizeDropDownList::widget(
      [
	  'name' => 'ciudades',
	  'value' => $ciudades_sel,
	  'items' => Ciudad::find()->select(['nombre'])
			  ->indexBy('id')
			  ->column(),
	  'options' => [
              'id' => 'ciudades',
              'multiple' => true,
	  ],
  ]) ?>

  <?= $form->field($model, 'introduccion')
	 ->textarea(['rows' => 6])
	 ->label("Breve introducción personal (tu historia)")
  ?>

  <div class="row">
    <div class="col-md-6">
      
      <?= $form->field($model, 'correo')
	     ->textInput([
		 'maxlength' => true,
		 'placeholder' => 'juan.perez@gmail.com'
	     ])
	     ->label('Correo electrónico
<span class="text-danger">*</span>')
      ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($model, 'tel')
	     ->textInput([
		 'maxlength' => true,
		 'placeholder' => '299 12345678',
	     ])
	     ->label('Teléfono
<span class="text-danger">*</span>')
      ?>
    </div>
  </div>
  <?= $form->field($model, 'mercado_pago')
	 ->checkbox([
	     'label' => 'Usas mercado pago o algún medio de pago online?
<span class="text-danger">*</span>',
	 ])
  
  ?>

  <?= $form->field($model, 'whatsapp')
	 ->textInput(['maxlength' => true])
	 ->label('Whatsapp
<span class="text-danger">*</span>')
  ?>

  <?= $form->field($model, 'facebook')
	 ->textInput(['maxlength' => true])
	 ->label("URL de Facebook (opcional)")
  ?>

  <?= $form->field($model, 'instagram')
	 ->textInput(['maxlength' => true])
	 ->label("URL de Instagram (opcional)")
  ?>

  <?php if (Yii::$app->user->can('colaborador-change_user_id')){ ?>
    <div class="panel panel-danger">
      <div class="panel-heading">
	<span class="glyphicon glyphicon-fire" />
	Admin
      </div>

      <div class="panel-body">
	<?= $form->field($model, 'usuario_id')->dropdownList(
	    User::find()->select(['username'])
		->indexBy('id')
		->column()
	); ?>
      </div>  
    </div>    
  <?php 
  }else{
      echo $form->field($model, 'usuario_id')->hiddenInput()->label(false);
  }
  ?>

  <?php
  if (!$editing){ 
      echo $form->field($model, 'verifyCode')
	       ->widget(yii\captcha\Captcha::className());
  }
  ?>
  
  <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
