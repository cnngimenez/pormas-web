<?php

/* @var $this yii\web\View */
/* @var $model app\models\Colaborador */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->razon_social;
$this->params['breadcrumbs'][] = ['label' => 'Tarjetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

// Crear el QR de esta IRI.
use Da\QrCode\QrCode;

$qrurl = Url::to(['colaborador/view', 'id' => $model->id], true);
$qrCode = (new QrCode($qrurl))->setSize(150)->setMargin(5);

$qrCode->writeFile(__DIR__ . '/QRCode.png');
header('Content-Type: ' . $qrCode->getContentType());

?>
<div class="colaborador-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('colaborador-update')){ ?>
      <div class="panel panel-danger">
	<div class="panel-heading">
	  <span class="glyphicon glyphicon-fire" />
	  Admin
	</div>
	<div class="panel-body">
	  <?= Html::a('Update', ['update', 'id' => $model->id],
		    ['class' => 'btn btn-primary']) ?>
          <?= Html::a('Delete', ['delete', 'id' => $model->id], [
              'class' => 'btn btn-danger',
              'data' => [
                  'confirm' => 'Are you sure you want to delete this item?',
                  'method' => 'post',
              ],
          ]) ?>
	  <?php
	  if (!$model->publico) {
	      echo Html::a('Aprobar y publicar',
			  ['publicar', 'id' => $model->id, 'value' => true ],
			  [ 'class' => 'btn btn-success' ]);
	  }else{
	      echo Html::a('Desaprobar y ocultar',
			  ['publicar', 'id' => $model->id, 'value' => false ],
			  [ 'class' => 'btn btn-danger' ]);
	  }
	  ?>
	</div>
      </div>
    <?php } ?>
    <p>
    <?= Html::a('Ver Tarjeta', ['tarjeta', 'id' => $model->id],
	      ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	    'publico:boolean',
	    'usuario.username',
            'razon_social',
            'ciudad',
            'actividad',
	    ['attribute' => 'rubro.nombre',
	     'label' => 'Rubro'],
            'descripcion:ntext',
            'introduccion:ntext',
            'correo:email',
            'mercado_pago:boolean',
            'whatsapp',
            'facebook',
            'instagram',
	    [ 'attribute' => 'ciudades',
	      'value' => function ($model) {
		  $nombres = [];
		  foreach ($model->ciudades as $ciudad){
		      $nombres[] = $ciudad->nombre;
		  }
		  return join (",", $nombres);
	      }
	    ],
	    [
		'attribute' => 'marca',
		'value' => function ($model) {
		    return $model->marca_URL;		
		},
		'format' => ['image', []],
	    ],
	    [
		'label' => 'QR',
		'value' => $qrCode->writeDataUri(),
		'format' => ['image', []],
	    ],
        ],
    ]) ?>
</div>
