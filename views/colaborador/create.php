<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Colaborador */

$this->title = 'Crear Tarjeta';
$this->params['breadcrumbs'][] = ['label' => 'Tarjeta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="colaborador-create">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>Por favor a continuación dejanos tus datos y nos pondremos en contacto con vos.</p>

  <p>
    Si no sabés como crear el link/botón de pago
    <a href="https://www.youtube.com/watch?v=DF8NSU55LIs&t=60s">hace click acá</a>
  </p>

  <p>Los campos marcados con asterisco (<span class="text-danger"> * </span>)
    son obligatorios.</p>
  
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>

</div>
