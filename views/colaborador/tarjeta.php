<?php

/* @var $model app\models\Colaborador */

use yii\helpers\Html;
use yii\helpers\Url;

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAB;
use rmrevin\yii\fontawesome\FAR;

use Da\QrCode\QrCode;

use app\assets\TarjetaAsset;
TarjetaAsset::register($this);

$qrurl = $model->friendly_url;
$qrCode = (new QrCode($qrurl))->setSize(150)->setMargin(5);

$qrCode->writeFile(__DIR__ . '/QRCode.png');
header('Content-Type: ' . $qrCode->getContentType());

?>
<?php
if (Yii::$app->user->can('colaborador-publicar') &&
    !$model->publico){
?>
  <div class="container">
    <div class="alert alert-danger">
      <span class="glyphicon glyphicon-fire" />
      Esta tarjeta no se encuentra aprobada para publicar.
      Usted puede verla por poseer los permisos necesarios.
    </div>
  </div>
<?php } ?>

<div class="header-tarj">
  <div class="container">
    <div class="header-left">
      
      <p class="rubro">
	<?= $model->actividad ?>
      </p>

      <h1 class="razon_social">
	<?= $model->razon_social ?>
      </h1>

      <p class="ciudad">
	<?= $model->ciudad ?>
      </p>
      
    </div>
    <div class="header-right">
      <?= Html::img($model->marca_URL, [
	  'class' => 'img-circle img-marca',
      ]); ?>      
    </div>

  </div>
</div>


<div class="servicios background-2">
  <div class="container">
    <h1 class="subtitle">Servicios</h1>
    <p class="text">
      <?= $model->descripcion ?>
    </p>
  </div>
</div>


<div class="opiniones strip-background">
  <div class="container">
    <h2 class="subtitle">Opiniones</h2>
    <p class="text"></p>
  </div>
</div>

<div class="background-2">
  <div class="container">
    <h2 class="subtitle">Contacto</h2>
    <p class="text contact-text">
      <img draggable="false" role="img" class="emoji" alt="👋"
	   src="https://s.w.org/images/core/emoji/12.0.0-1/svg/1f44b.svg"
	   data-src="https://s.w.org/images/core/emoji/12.0.0-1/svg/1f44b.svg">
      ¡Hola! Podés contactarnos por los siguientes medios:
    </p>
    <div class="row contact-buttons">
      <div class="col-xs-2">	
      </div>
      <div class="col-xs-2">
	<?php
	if ($model->whatsapp != "" and $model->whatsapp != null){
	    echo Html::a(FAS::icon('phone'),
			'tel:' . $model->whatsapp,[
			    'class' => 'contact-button'
	    ]);
	} ?>
	
      </div>
      <div class="col-xs-2">
	<?php if ($model->whatsapp != "" and $model->whatsapp != null){
	    echo Html::a(FAB::icon('whatsapp'),
			'https://wa.me/' . $model->whatsapp,[
			    'class' => 'contact-button'
	    ]);
	}?>
      </div>
      <div class="col-xs-2">
	<?php
	if ($model->facebook != "" and $model->facebook != null){
	    echo Html::a(FAB::icon('facebook'),
			$model->facebook,[
			    'class' => 'contact-button'
	    ]);
	}?>	
      </div>
      <div class="col-xs-2">
	<?php if ($model->instagram != "" and $model->instagram != null){
	    Html::a(FAB::icon('instagram'),
		   $model->instagram,[
		       'class' => 'contact-button'
	    ]);
	} ?>
      </div>
      <div class="col-xs-2">
      </div>
    </div>
  </div>
</div>


<div class="voucher strip-background">
  <?= $this->render('_carousel', [
      'model' => $model
  ]); ?>
</div>

<div class="historia background-2">
  <div class="container">
    <h2 class="subtitle">Mi historia</h2>
    <p class="text">
      <?= $model->introduccion ?>
    </p>
  </div>
</div>

<div class="comparti strip-background">
  <div class="container">
    <p class="text">¡Compartí mi tarjeta! 🙂</p>


    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4 text-center">
	<div class="sharer-outline wp-outline">
	  <span class="sharer-inline wp-inline">
	    <a href="<?=
		     "https://api.whatsapp.com/send?text=*" .
		     $model->razon_social . "| Por Más* " .
		     $model->descripcion . " " .
		     $model->friendly_url
		     ?>"
	       class="nobs-link">
	      <?= FAB::icon('whatsapp') ?>
	    </a>
	  </span>
	</div>
	<div class="sharer-outline fb-outline">
	  <span class="sharer-inline fb-inline">
	    <a href="<?=
		     "https://www.facebook.com/sharer.php?u=" .
		     $model->friendly_url		     
		     ?>"
	       class="nobs-link">
	      <?= FAB::icon('facebook') ?>
	    </a>
	  </span>
	</div>
	<div class="sharer-outline telegram-outline">
	  <span class="sharer-inline telegram-inline">
	    <a href="<?=
		     "https://telegram.me/share/url?url=" .
		     $model->friendly_url .
		     "&text=" .
		     "*" . $model->razon_social . " | Por Más* " 
		     ?>"
	       class="nobs-link">
	      <?= FAB::icon('telegram') ?>
	    </a>
	  </span>
	</div>
	<div class="sharer-outline tweet-outline">
	  <span class="sharer-inline tweet-inline">
	    <a href="<?=
		     "https://twitter.com/intent/tweet?text=" .
		     $model->razon_social . "| Por Más :" .
		     $model->friendly_url
		     ?>"
	       class="nobs-link">
	      <?= FAB::icon('twitter') ?>
	    </a>
	  </span>
	</div>
	<div class="sharer-outline linkedin-outline">
	  <span class="sharer-inline linkedin-inline">
	    <a href="<?=
		     "https://www.linkedin.com/shareArticle?mini=true&url=" .
		     $model->friendly_url .
		     "&title=" .
		     $model->razon_social . "| Por Más" .
		     "&summary=" .
		     $model->descripcion .
		     "&source=" . 
		     $model->friendly_url
		     ?>"
	       class="nobs-link">
	      <?= FAB::icon('linkedin') ?>
	    </a>
	  </span>	  
	</div>
	<div class="sharer-outline email-outline">
	  <span class="sharer-inline email-inline">
	    <a href="<?=
		     "mailto:" .
		     "?subject=" .
		     $model->razon_social . " | Por Más" .
		     "&body=" .
		     $model->razon_social . "\n" .
		     $model->descripcion . "\n\n" .
		     $model->friendly_url
		     ?>"
	       class="nobs-link">
	      <?= FAS::icon('envelope') ?>
	    </a>
	  </span>
	</div>

	<br/>
	<?= Html::img($qrCode->writeDataUri(), [
	    'class' => 'img-rounded img-responsive qr-share',
	])?>
	
      </div>    	
    </div>
    <div class="col-md-4">           
    </div>
  </div>
</div>

<div class="quiero background-2">
  <div class="container">

    <p>
      <?=
      Html::a('¡Quiero mi tarjeta <b>GRATIS</b>! '.
	     FAR::icon('arrow-alt-circle-right'),
	     ['site/index'],
	     ['class' => 'quiero-link']);
      ?>
    </p>
    <p>
      <?=
      Html::a('Informar problema',
	     'https://pormas.org/reportar',
	     ['class' => 'informar-fallo'])
      ?>
    </p>
  </div>
</div>
