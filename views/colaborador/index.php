<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ColaboradorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use rmrevin\yii\fontawesome\FAS;

$this->title = 'Tarjetas';
$this->params['breadcrumbs'][] = $this->title;

$buttons = [
    'card' => function ($url, $model, $key) {
        return  Html::a(FAS::icon('id-card'),
		  Url::to(['colaborador/tarjeta', 'id' => $model->id]));
    }
]
?>
<div class="colaborador-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <?php if (Yii::$app->user->can('colaborador-create')) { ?>
    <div class="panel panel-danger">
      <div class="panel-heading">
	<span class="glyphicon glyphicon-fire" />
	Admin
      </div>
      <div class="panel-body">
	<?= Html::a('Create Colaborador', ['create'], ['class' => 'btn btn-success']) ?>
      </div>
    </div>
  <?php } ?>

  <?php Pjax::begin(); ?>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <div class="table-responsive">
    
    <?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'rowOptions' => function ($model, $index, $widget, $grid) {

            return [
		'id' => $model['id'],
		'onclick' => 'location.href="'
			. Yii::$app->urlManager->createUrl('colaborador/view')
			. '?id="+(this.id);'
            ];
	},
	'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'razon_social',
            // 'marca',
            'actividad',
	    ['attribute' => 'rubro.nombre',
	     'label' => 'Rubro'],
	    'publico:boolean',
            ['attribute' => 'descripcion',
	     'format' => 'ntext',
	     'contentOptions' => [
		 'style' => 'min-width: 400px'
	     ],
	    ],
            ['attribute' => 'introduccion',
	     'format' => 'ntext',
	     'contentOptions' => [
		 'style' => 'min-width: 400px',
	     ],
	    ],
            'correo:email',          
            'mercado_pago:boolean',
            'whatsapp',
            'facebook:url',
            'instagram:url',
	    [ 'attribute' => 'ciudades',
	      'value' => function ($model) {
		  $nombres = [];
		  foreach ($model->ciudades as $ciudad){
		      $nombres[] = $ciudad->nombre;
		  }
		  return join (",", $nombres);
	      }
	    ],

            ['class' => 'yii\grid\ActionColumn',
	     'buttons' => $buttons,
	     'template' => '{card}{view}{update}{delete}'],
	],
    ]); ?>

  </div><!-- .table-responsive -->
  
  <?php Pjax::end(); ?>

</div>
