
<?php
use yii\bootstrap\Carousel;

use yii\helpers\Html;
use yii\helpers\Url;


$items = [];
foreach ($model->vouchers as $voucher){

    $text = $this->render('_voucher', [
	'model' => $voucher
    ]);
   
    $items[] = [
	// 'content' => Html::img($voucher->foto_URL),
	'content' => $text, 
	// 'caption' => 
    ];
}

?>

  <?= Carousel::widget([
      'items' => $items,
      'showIndicators' => true,
      'options' => [
	  'class' => 'carousel slide'],
      'controls' => [
	  '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	<span class="sr-only">Anterior</span>',
	  '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	<span class="sr-only">Siguiente</span>',
      ],
  ]); ?>

