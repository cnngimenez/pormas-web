<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VoucherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vouchers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voucher-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?php
    if ($colab->puede_crear_voucher() ||
	Yii::$app->user->can('user-management')){
	echo Html::a('Agregar Voucher', ['create'], ['class' => 'btn btn-success']);
    }
    ?>
  </p>

  <?php Pjax::begin(); ?>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php if (Yii::$app->user->can('user-management')){ ?>
    <div class="alert alert-danger">
      <span class="glyphicon glyphicon-fire" />
      Debido a su permiso de gestor de usuarios puede: ver todos los voucher y
      revisar el id y el colaborador.
    </div>      
  <?php } ?>

  <div class="table-responsive">
    
    <?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [ 'attribute' => 'id',
	      'visible' => Yii::$app->user->can('user-management'),
	    ],
            'servicio',
	    'precio',
            'con_promo:boolean',
            ['attribute' => 'promo_descuento',
	     'label' => 'Precio promocional',
	    ],
	    ['attribute' => 'descripcion',
	     'contentOptions' => [
		 'style' => 'min-width: 400px; max-width: 500px',
	     ],
	    ],
            ['attribute' => 'mercado_pago',
	     'format' => 'url',
	     'contentOptions' => [
		 'style' => 'max-width:200px; text-overflow: ellipsis; overflow: hidden;',
	     ],
	    ],
            //'foto',
	    ['attribute' => 'colaborador.razon_social',
	     'visible' => Yii::$app->user->can('user-management'),
	    ],
            ['attribute' => 'colaborador_id',
	     'visible' => Yii::$app->user->can('user-management'),
	    ],

            ['class' => 'yii\grid\ActionColumn'],
	],
    ]); ?>
    
  </div><!-- .table-responsive -->
  
  <?php Pjax::end(); ?>

</div>
