<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VoucherSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="voucher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'servicio') ?>

    <?= $form->field($model, 'precio') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'mercado_pago') ?>

    <?php // echo $form->field($model, 'con_promo') ?>

    <?php // echo $form->field($model, 'promo_descuento') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'colaborador_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
