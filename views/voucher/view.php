<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Voucher */

$this->title = $model->servicio;
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="voucher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
      <?= Html::a('Editar', ['update', 'id' => $model->id],
		['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está segura/o?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'servicio',
            'precio',
            'descripcion',
            'mercado_pago',
            'con_promo:boolean',
            'promo_descuento',
            [
		'attribute' => 'foto',
		'value' => function ($model) {
		    return $model->foto_URL;
		},
		'format' => 'image',
	    ],
            // 'colaborador_id',
        ],
    ]) ?>

</div>
