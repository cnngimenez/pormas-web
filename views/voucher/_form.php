<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Voucher */
/* @var $form yii\widgets\ActiveForm */

use kartik\number\NumberControl;

?>

<div class="voucher-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'servicio')->textInput(['maxlength' => true]) ?>

  <div class="row">
    <div class="col-md-6">
      <?= $form->field($model, 'precio')->widget(
	  NumberControl::className(),
	  [
	      'maskedInputOptions' => [
		  'prefix' => '$ ',
		  'groupSeparator' => '.',
		  'radixPoint' => ','
	      ],
	  ]
	  
      ) ?>      
    </div>
    <div class="col-md-6">
      <?= $form->field($model, 'con_promo')->checkbox() ?>

      <?= $form->field($model, 'promo_descuento')->widget(
	  NumberControl::className(),
	  [
	      'maskedInputOptions' => [
		  'prefix' => '$ ',
		  'groupSeparator' => '.',
		  'radixPoint' => ','
	      ],
      ]) ?>
    </div>
  </div>

  <?= $form->field($model, 'descripcion')->textArea(['maxlength' => true]) ?>

  <?= $form->field($model, 'mercado_pago')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'foto_image')->fileInput() ?>

  <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
