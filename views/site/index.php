<?php

/* @var $this yii\web\View */

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Colaborador;
use app\models\Ciudad;
use app\models\Rubro;
// use yii\widgets\ListView;
use \nerburish\masonryview\MasonryView;

$this->title = 'pormás';

$colab = Colaborador::find()->where(['usuario_id' => Yii::$app->user->id])->one();

?>
<div class="site-index">

  <div class="jumbotron">
    <h1>Por Más</h1>

    <p class="lead">Comprá hoy, recibí el producto o servicio cuando
      termine la cuarentena.</p>

    <p>
      <?php
      if (Yii::$app->user->isGuest){
	  echo Html::a('¡Quiero mi tarjeta!',
		      ['colaborador/create'],
		      ['class' => 'btn btn-lg btn-success']);
      }else{
	  if ($colab != null){
	      echo Html::a('Ver mi tarjeta',
			  $colab->friendly_url,
			  ['class' => 'btn btn-lg btn-primary']);
	      echo " ";
	      echo Html::a('Editar mi tarjeta',
 			  ['colaborador/update', 'id' => $colab->id],
			  ['class' => 'btn btn-lg btn-default']);
	  }
      }
      ?>
    </p>
  </div>

  <div class="body-content">    

    <?php
    Pjax::begin();    

    $form = ActiveForm::begin([
	'options' => ['data' => ['pjax' => true]],
    ]);
    ?>
    
    <div class="panel panel-primary">
      <div class="panel-heading">
	<h4>
	  <span class="glyphicon glyphicon-search" />
	  Buscá servicios y productos
	</h4>
      </div>
      <div class="panel-body">	
	<p>
	  Elegí a quién contratar filtrando por rubro y ciudad,
	  podrás ver opiniones de otras personas sobre los profesionales
	  que vas a contratar.
	</p>      

	<div class="row">
	  <div class="col-md-2">
	    <?= Html::dropDownList('ColaboradorSearch[rubro_id]',
				 $searchModel->rubro,
				 ['-1' => 'Rubro'] + 
				 Rubro::find()
				      ->select(['nombre'])
				      ->indexBy('id')
				      ->column()
				,['class' => 'form-control input-lg']) ?>
	  </div>
	  <div class="col-md-2">
	    <?= Html::dropDownList('ColaboradorSearch[ciudad_servicio_id]',
				 $searchModel->ciudad_servicio_id,
				 ['-1' => 'Ciudad'] + 
				 Ciudad::find()
				       ->select(['nombre'])
				       ->indexBy('id')
				       ->column()
			       , ['class' => 'form-control input-lg']) ?>
	  </div>
	  <div class="col-md-6">
	    <?= Html::textInput('ColaboradorSearch[texto]',
			      $searchModel->texto,
			      [
				  'class' => 'form-control input-lg',
				  'placeholder' => 'Nombre o palabra clave a buscar',
	    ]) ?>
	  </div>
	  <div class="col-md-2">
	    <?= Html::submitInput('Buscar', 
				[ 'class' => 'btn btn-lg btn-success'] )?>
	  </div>
	</div>
	
      </div>
    </div>
    
    <?php ActiveForm::end(); ?>
    

    <?= MasonryView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_colaborador',
	'clientOptions' => [
	    'columnWidth' => 340,
	    'gutter' => 15,
	],
	// 'afterItem' => 'function () { console.log("hello"); $("#w1 .grid").masonry(); }',
	'cssFile' => [
	    "@web/css/site_colab.css"		
	]
    ]) ?>


    <?php Pjax::end(); ?>
    <div class="masonry-actualizar">
    </div>    
    
    <?php if (Yii::$app->user->isGuest){ ?>
      <h1 id="quiero">¿Necesitás una tarjeta?</h1>
      <p>Te ayudamos a generar ingresos hoy mismo con un servicio que prestarás en un
	futuro, también podrás recibir recomendaciones de tus clientes y posicionar
	tu tarjeta en Google.</p>

    <?php } ?>
  </div>
</div>
