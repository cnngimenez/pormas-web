<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="colab-card">
  <div class="thumbnail">
    <?= Html::img($model->marca_URL, [
	// 'height' => '140px',
	'class' => 'img-rounded img-responsive'
    ]); ?>

    <div class="caption">
      <h3><?= $model->razon_social ?></h3>
      <p><?= HtmlPurifier::process($model->descripcion) ?></p>
      <p><?= Html::a ('Ver Tarjeta',
		    $model->friendly_url,
		    ['class' => 'btn btn-primary']) ?>
    </div>
  </div>
</div>
